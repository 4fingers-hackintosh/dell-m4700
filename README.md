# Dell M4700 Hackintosh

## My system specs:
- CPU: Intel Core i7 - 3720QM (notice that **I'm not sure other variants** will work with this EFI, so you have to make your own configuration if you use *3740QM*, etc...)
- RAM: 16 GB (8GB x 2) 1600 MHz DDR3L, at least 8GB is recommended for best experiences.
- Storage: 120 GB SATA SSD (for system and applications must install in here only) + 640GB SATA Disk (after installation, I'll move **Users** folder to here, for using separated Applications folder, you don't have to follow this setup).

I pinned Applications folders on HDD to Favourites and install third party applications in here.

![Two Applications folders](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/first-applications-folder-is-on-640g-hdd.png)

Storage in About This Mac

![Storage in About This Mac](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/storage.png)

Storage in System Report

![Storage in System Report](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/storage-in-system-report.png)

- iGPU: Intel HD4000
- GPU: NVIDIA Quadro K1000M (Kepler structure, native in macOS)

![GPU](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/graphics-display-in-system-report.png)

- Screen: 1920x1080 60Hz
- Audio: IDT92HD93BXX
- Wifi: Replaced to BCM943224 HMS (No Bluetooth) (if you don't use this, please remove AirportBrcmFixup.kext)
- Bluetooth: Built-in
- Camera: Built-in

## Patchs applied:

### DSDT/SSDT:
- ECRW (in a _CRS method) in [Guide paching laptop DSDT, SSDT](https://www.tonymacx86.com/threads/guide-patching-laptop-dsdt-ssdts.152573/)
- Fix *pnp/pnp lower case error (rehabman laptop repo)

### Clover hotpatch explanations

#### Graphics (not really need if you don't disable NVIDIA)
- ig-flatform-id: 0x01660004 (HD4000) and Dual Link = 1
- Inject Intel only
- Nvidia not supported in Optimus configuration, so injecting Nvidia not helpful ([Source](https://www.tonymacx86.com/threads/installing-on-dell-precision-m4700-bunch-of-issues.208314/#post-1383146))

#### Kernel and kext patchs:
- USB Port Limits (checkout at **Kernel and Kext Patches > Kexts to Patch**)

### Kexts:
- Enable wifi: Lilu + AirportBrcmFixup
- Enable ~~Bluetooth~~ (not worked any more since I upgrade to 10.15.1)
- Enable audio: use "change HDAS to HDEF" hotpatch in config.plist + Lilu + AppleALC + inject layout-id 12 in config.plist / Devices
- All kexts should be updated to the lastest version via [Kext Updater](https://bitbucket.org/profdrluigi/kextupdater/downloads/) to work perfectly.
- Enable other devices using USB connect: build your own USBMap.kext. Read [here](https://github.com/corpnewt/USBMap) for more details.

## Patchs not applied:
- Sleep.
- More if you find out ...

![About](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/about.png)

## Change logs:
- **December 10th, 2019**: There's a few changes to `config.plist` and DSDT to make it work on 10.15.1. I added `SSDT-EC.aml` (after too much times stucked at panic, i found this [thread](https://www.tonymacx86.com/threads/update-directly-to-macos-catalina.284463/page-6#post-2015067)) to `ACPI/patched` and new DSDT, SSDT patches. Some kexts removed because I found that it's not necessary. I'm not ensure it will work completely for 10.14.x, just use the old commit before.
- **March 25th, 2020**: Apple released macOS 10.15.4. I added **Kernel and Kext Patches > AppleIntelCPUPM** to config.plist to avoid panic.
- **April 14th, 2020**: Fixed several issues replacing FakeSMC with VirtualSMC and its complements.

For more information or have a problem or you want to donate me a cup of ☕, please contact me at Telegram: [@x4fingers](https://t.me/x4fingers)